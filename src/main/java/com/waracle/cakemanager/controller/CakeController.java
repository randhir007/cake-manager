package com.waracle.cakemanager.controller;

import com.waracle.cakemanager.entity.Cake;
import com.waracle.cakemanager.service.CakeService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cakes")
@Slf4j
public class CakeController {

    @Autowired
    private CakeService cakeService;

    @GetMapping
    @PreAuthorize("hasRole('USER')")
    @Operation(summary = "Gets all cakes")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Get All Cakes", content = {@Content(mediaType = "application/json")})
    })
    public ResponseEntity<List<Cake>> getAllCakes() {
        log.info("Get All Cakes");
        List<Cake> cakes = cakeService.getAllCakes();
        return ResponseEntity.ok(cakes);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Add Cake")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Cake added", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "403", description = "Forbidden. Cake can't be added", content = {@Content(mediaType = "application/json")})
    })
    public ResponseEntity<Cake> addCake(@RequestBody Cake cake) {
        Cake newCake = cakeService.addCake(cake);
        log.info("Cake added::" + cake.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(newCake);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Updates cake details in database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cake updated", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Cake not found", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "403", description = "Only Admins cann update cake", content = {@Content(mediaType = "application/json")})
    })
    public ResponseEntity<Cake> updateCake(@PathVariable Long id, @RequestBody Cake cake) {
        Cake updatedCake = cakeService.updateCake(id, cake);
        log.info("Cake Updated for Id: " + id);
        return ResponseEntity.ok(updatedCake);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Deletes cake from database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Cake deleted successfully", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", description = "Cake not found in database to update", content = {@Content(mediaType = "application/json")}),
            @ApiResponse(responseCode = "403", description = "Users other than Admins cannot delete cake", content = {@Content(mediaType = "application/json")})
    })
    public ResponseEntity<Void> deleteCake(@PathVariable Long id) {
        cakeService.deleteCake(id);
        log.info("Cake deleted for Id: " + id);
        return ResponseEntity.noContent().build();
    }
}

