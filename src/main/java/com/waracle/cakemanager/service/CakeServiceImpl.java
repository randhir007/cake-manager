package com.waracle.cakemanager.service;

import com.waracle.cakemanager.entity.Cake;
import com.waracle.cakemanager.exceptions.CakeNotFoundException;
import com.waracle.cakemanager.repository.CakeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CakeServiceImpl implements CakeService {

    @Autowired
    private CakeRepository cakeRepository;

    public List<Cake> getAllCakes() {
        log.info("Get All Cakes");
        return cakeRepository.findAll();
    }

    public Cake addCake(Cake cake) {
        log.info("Adding new cake: " + cake.getName());
        return cakeRepository.save(cake);
    }

    public Cake updateCake(Long id, Cake updatedCake) {
        log.info("Updating Cake Id: " + id);
        Cake cake = getCakeById(id);
        cake.setName(updatedCake.getName());
        cake.setDescription(updatedCake.getDescription());
        cake.setImage(updatedCake.getImage());
        return cakeRepository.save(cake);
    }

    public void deleteCake(Long id) {
        log.info("Deleting Cake Id: " + id);
        Cake cake = getCakeById(id);
        cakeRepository.delete(cake);
    }

    private Cake getCakeById(Long id) {
        return cakeRepository.findById(id).orElseThrow(() -> new CakeNotFoundException("Cake not found with id " + id));
    }
}

